# Hardware

![Block Diagram](Block_Diagram.png)

TODO
[ ] Memory has now been moved from after the DC to before the DC. Memory has all ALU (outputs and forwarded outputs) passed into it and only looks at 

## Overview

The processor is a 16 bit 5-stage pipeline with forwarding.

## Quick Definition of Terms

__Signal:__ A physical connection between two devices. All signals are digital logic and are either in a high state, which corresponds to a logic "1" or a low state which corresponds to a logic "0".

__Signal Harness / Signal Bus:__ A group of signals that are grouped together to by likeness. This is often relevant when talking parallel outputs. If the contents of a 3 bit register are require to be output, the parallel output interface is a signal harness of three signals, with each signal corresponding to a bit in the register.

__Data:__ Information that the pipeline is processing. Data is not to be confused with instructions or modes. An example of pipeline data is the contents of a register.

__Instruction:__ A command that tells the processor how to behave. It is usually in regards to how to behave with respect to specific data.

__Mode:__ A specific pipeline stage operation. A mode is not to be confused with an instruction. Instructions are for the entire processor, modes are specific to the individual stages of the pipeline that are required to complete the instruction. Example: an instruction might be ADD R1, R2, R3. ADD is the instruction, but the pipeline breaks down ADD to an ALU-specific mode that is ADD and a DC-specific mode that is write back operation into R1.

__Pipeline Stage:__ A component of a pipeline that helps complete an instruction.

__Pipeline Operational Unit (POU):__ A component of a pipeline stage that controls Pipeline Compute Units.

__Pipeline Compute Unit (PCU) :__ A component of a pipeline stage that performs operations from a given instruction. It does not modify the instruction in any way and is considered to be the end point for that instruction.

__Central Control Unit (CCU):__ A component of the CPU that monitors each stage of the pipeline and checks for hazards. It is able to issue a No Operation (NOP) command into each stage to mitigate hazards.

A pipeline stage usually consists of an POU, PCU and a latch together. The POU takes an instruction input and modifies that instruction so that the PCU can operate from that given instruction. It is important to note that the POU never generates data the data the PCU is operating from, it is simply issuing commands relevant to that PCU and forwarding the relevant data to it. The latch is there to simplify timing issues and allow for the CCU to monitor the current state of each stage.

## Instruction Fetch / Instruction Decode (IF/ID)

![Instruction Fetch](IF.png)

### IF/ID Description

Instruction Fetch / Instruction Decode (IF/ID) is a POU that acts as a gateway to the pipeline. The IF component imports instructions corresponding to the current value of the Program Counter (PC) from ROM and forwards them into the ID stage. The Instruction Decode stage takes the 16 bit instruction and decodes it into the opcode and the operands. The PC is inside the IF/ID POU and is not accessed by any other POU or the CCU. The PC is incremented sequentially unless a Format B instruction is executed, in which case it modifies the value of the PC.

All instructions are 16 bit. The most significant 7 bits are the opcode and the remaining 9 bits are the operands. Exact format of operands and their expected content depends on the instruction format.

### IF/ID Input Signals

- __pc_out〈15..0〉:__ ROM address of the next instruction. Instruction are incremented sequentially unless a branch needs to be handled.
- __data〈15..0〉:__ 16 bit instruction from ROM.
- __br_en:__ Harness from ALU that alerts the If/ID that the contents of pc have been updated. This harness is active high.
- __br_pc〈15..0〉:__Harness containing the updated value of PC from the ALU. The contents of this harness is immediately used to update the PC and the current fetched instruction is discarded.

### IF/ID Output Signals

- __rd_index1〈2..0〉:__ The RF index that data is to be read from and placed on rd_data1
- __rd_index2〈2..0〉:__ The RF index that data is to be read from and placed on rd_data2
- __wr_index〈2..0〉:__ The RF index that data is being written to from the wr_data harness
- __wr_data〈2..0〉:__ Harness that contains data for a write operation
- __wr_en:__ Harness that tells the RF that the data relevant to a write operation is current and it needs to perform the write command
- __wb_index〈2..0〉:__ A harness that is forwarded through the RF to the IE. It contains the index of a register that is to be written back to later on.
- __op_code〈7..0〉:__ Harness that contains the current operation. The RF only checks if there is a MOV command on this harness, otherwise it is forwarded through.
- __cl〈15..0〉:__ Harness that contains additional operands that are not supplied from the RF. This harness is forwarded to the IE.

Note that even though all values are forwarded to RF, not all values are needed by RF. The RF forwards on wb_index, op_code and cl.

#### Format A

- __NOP:__  _No Operation_
- __ADD:__ _ADD ra,rb,rc: Adds rb to rc and stores result in ra_
- __SUB:__ _SUB ra,rb,rc: Subtracts rb from rc and stores result in ra_
- __MUL:__ _MUL ra,rb,rc: Multiplies rb and rc, stores result in ra_
- __NAND:__ _NAND ra,rb,rc: rb NAND rc store result in ra_ 
- __SHL:__ _SHL ra#n: Shift ra left by n_
- __SHR:__ _SHL ra#n: Shift ra right by n_
- __TEST:__ _TEST ra: Tests ra to see if the value is non-zero and non-negative_
- __OUT:__ _OUT ra: Outputs the contents of ra on the output harness_
- __IN:__ _IN ra: Loads the contents of the in signal harness into ra_

NOP instructions are sent to Instruction execute as a NOP opcode. From there the IE forwards a NOP to Data Controller.

ADD, SUB, MUL and NAND instructions are stripped down to their opcodes and operands. The opcode is directly forwarded to the Instruction Execute over the op_code harness. The second and third operands are decoded and placed on the RF read harness. The first operand is placed on the wb_index harness. The next clock cycle the RF reads the operands and outputs the contents of the registers. The opcode and wb_index are simply forwarded through and are available at the same time as the operands to simplify timing issues. The IE now has the opcode and knows what the data on rd_data1 and rd_data2 is for and can execute the instruction appropriately. It is important to note that almost all Format A instructions have operands that are the contents of specific registers. To clarify, the IF/ID stage is able to start the read for operands and the operands are available next clock cycle with the opcode at the IE.

SHL and SHR instructions are decoded and the operand is placed on the op_code harness. SHL and SHR instructions require a separate harness cl〈15..0〉in order to forwarded the amount to shift by tp the IE. To save a register file read/write operation, the instruction fetch forwards the amount to shift by directly to the IE via the cl harness. The IF/ID then places the first operand on the rd_index1 and wb_index harnesses. The next clock cycle the IE has the shift opcode, the value to shift on the rd_data1 harness and the amount to shift by on the cl harness.

OUT instruction is forwarded as an OUT opcode to the IE. The IF/ID also starts the read from RF and the opcode and operand are available for the IE the next clock cycle.

IN instruction is handled entirely by the IF/ID stage, it takes the data on the in harness and writes it directly to the RF. The IN opcode is made available on th opcode harness and effectively acts as a NOP to later stages.

#### Format B

- __BRR:__ BRR +disp.l: Increment the program counter by the amount specified by (disp.l*2)
- __BRR.N:__ BRR.N +disp.l: Increment the program counter by the amount specified by (disp.l*2) if the last instruction resulted in the negative flag of the ALU == TRUE.
- __BRR.Z:__ BRR.Z +disp.l: Increment the program counter by the amount specified by (disp.l*2) if the last instruction resulted in the zero flag of the ALU == TRUE.
- __BR:__ BR ra+disp.s: Add (displ*2) to the contents of ra, increment the program counter by the result.
- __BR.N:__ BR.N ra+disp.s: If the last instruction resulted in the negative flag of the ALU == TRUE, add (disp.s*2) to the contents of ra and increment the program counter by the result.
- __BR.Z:__ BR.Z ra+disp.s: If the last instruction resulted in the zero flag of the ALU == TRUE, add (disp.s*2) to the contents of ra and increment the program counter by the result.
- __BR.SUB:__ BR.SUB ra+disp.s: Saves (PC value + 1) into r7 the adds the contents of ra and disp.s and increments the program counter by that amount.
- __RETURN:__ Copies the contents of r7 into the PC.

All Format B instructions result in the PC being updated. The PC is updated either internally as per the BRR instruction or its updated via the br_pc harness. This harness is updated concurrently, meaning as soon as the br_en is pulled high the value of the br_pc is added to the PC. It is important to note that the If/ID does not handle data hazards with branching. The controller is responsible for issuing NOPs and preventing hazards from happening.

BRR instruction is handled entirely by the IF/ID. At the decode stage, when ID sees a BRR instruction it increments the PC by the displacement (value of the operand). The IF/ID then forwards on a NOP over the op_code harness.

BRR.N and BRR.Z instructions are decoded and the opcode forwarded to the IE via the op_code harness. The displacement is decoded and placed on the cl harness. The next clock cycle the IE gets the opcode and the displacement.

BR, BR.N and BR.Z instructions are decoded and the opcode forwarded on the op_code harness. The IF/ID decodes the operands and places Ra on the rd_index1 harness. It then takes the displacement and places it on the cl harness. The IF/ID then issues a read to RF and the contents of Ra, the displacement and opcode are available to the IE at the next clock cycle.

BR.SUB instruction is decoded and the opcode forwarded on the op_code harness. The IF/ID places Ra on the rd_index1 harness. It also places the contents of PC + 1 on the wr_data harness. It places R7 on the wr_index harness and brings the wr_en harness high. This enables the RF to write the contents of PC+1 into R7 and simultaneously read the contents of ra to the IE. It also places the displacement on the cl harness. The next clock cycle the IE has the contents of Ra, the displacement and the opcode.

RETURN instruction is decoded and the opcode forwarded on the op_code harness. The IF/ID then places R7 on the rd_index1 harness. The next clock cycle the IE has the contents of R7 and the opcode. The IF/ID then NOPs until br_en harness is pulled high. It then copies the contents of br_pc harness into PC and resumes operation as normal.

#### Format L

- __LOAD:__  LOAD r[dst], r[src]: Load the contents of memory specified by the memory address in r[src] and store into r[dst]
- __STORE:__ STORE r[dst], r[src]: Store the contents of r[src] into a memory location specified by r[dst]
- __LOADIMM:__ LOADIMM.upper #n: Load a value specified by the 8 least significant bits of the instruction into either the most significant bits or least significant bits (specified by bit 8 of the instruction) into r7
- __MOV:__ MOV r[dst], r[src]: Copy the contents of r[src] into r[dst]

LOAD instruction is decoded and the LOAD opcode is forwarded to the IE via the op_code harness. The IF/ID issues a read on the rd_index1 harness to the RF for the r[src] register. The IF/ID also takes the second operand r[dst] and places this register index onto the wb_index harness. The next clock cycle the IE gets the memory address (from r[src] on rd_data1) and the opcode telling it the current instruction is load. This is then forwarded down the pipeline as a memory read command with the wb_index.

STORE instruction is decoded and the STORE opcode is forwarded to the IE via the opcode harness. The IF/ID issues a read to the RF for the r[src] and the r[dst] registers. r[src] is placed on the rd_index1 harness and r[dst] is placed on the rd_index2 harness. The next clock cycle the IE gets the register contents and the opcode for the store instruction. The r[src] contents is the data to be written and r[dst] contents is the memory address to store the data. Contents of r[src] is made available on rd_data1 and r[dst] is made available on rd_data2. The IE takes rd_data1 data and forwards it onto the dc_data harness and takes the rd_data2 data and forwards it onto the mem_addr harness. It then issues a memory write command to the DC.

LOADIMM instruction is decoded and the LOADIMM opcode is forwarded on the op_code harness and the operand is place cl harness. The IF/ID places R7 ("111") on the rd_index1 and wb_index harnesses. The next clock cycle the IE has both the opcode, operand and the contents of R7.

MOV instruction is decoded and the MOV opcode is forwarded to the RF via the op_code harness. The IF/ID places the r[src] on read1 harness and r[dst] on wb_index harness. The next clock cycle the IE has the opcode, contents of r[src]and the write back index for the register. The rest of the pipeline handles it as a write back.

## Register File (RF)

TODO: Insert RF Image

### RF Description

The Register File is a Pipeline Compute Unit (PCU) that contains 7, 16 bit registers. The registers are used as general purpose registers with the exception of R7, which is used to save the PC state.

The RF has read and write interfaces to enable registers to be written to and read from. The RF has duel read interfaces that can be used concurrently. Writing is forced to the clock and can only be done once per clock cycle.

### RF Input Signals

#### RF Interface

These inputs are for the actual RF control.

- __rd_index1〈2..0〉:__
- __rd_index2〈2..0〉:__
- __wr_index〈2..0〉:__
- __wr_data〈15..0〉:__
- __wr_en:__

#### RF Write Back Interface

- __wb_index〈2..0〉:__
- __wb_data〈2..0〉:__
- __wb_en〈2..0〉:__

#### RF Passed Through Inputs

These inputs are not used by the RF and just passed through to prevent timing issues.

- __in_opcode〈2..0〉:__
- __in_wb_index〈2..0〉:__
- __in_cl〈2..0〉:__
- ~~__in_br_base〈2..0〉:__~~

### RF Output Signals

TODO: RF Output Signals

### RF Logic

TODO: RF Logic

## Instruction Execute (IE)

![Instruction Execute](IE.png)

### IE Description

The Instruction Execute is a POU that executes instructions. It takes a decoded instruction from op_code and remaps data from rd_data1, rd_data2 and cl into operands relevant to the current instruction on op_code. Once the outputs for the current instruction have been set, the outputs that are not set for the instruction are reset to a low state. This enables simpler control overhead for the CCU.

IE operates per clock cycle. Each clock rising edge it reads its inputs and on the falling edge the results are place on the output.

### IE Input Signals

- __rd_data1〈15..0〉:__ First RF data output.
- __rd_data2〈15..0〉:__ Second RF data output.
- __wb_index〈2..0〉:__ RF address for write back operations. IE does not actually need the rd_adr contents, it just forwards it next stage. Note that this signal can be connected directly to the DC with multiple buffers.
- __op_code〈6..0〉:__ The opcode for the current instruction. The IE stage takes this opcode and breaks it into an alu_mode instruction or dc_mode instruction.
- __cl〈15..0〉:__ The value to shift by. Since the format of a shift instruction is different the ID stage separates the shift instruction into opcode, operand and cl. For LOADIMM, the relevant data is forwarded on this harness.

### IE Output Signals

- __op_code〈6..0〉:__ Command that is forwarded to later pipeline stages
- __alu_in1〈15..0〉:__ The main ALU data input, can be considered to be the first operand
- __alu_in2〈15..0:__ The second ALU data input, can be considered to be the second operand
- __wb_index〈2..0〉:__ RF address that data is being written back to. It is important to note that the RF has a separate write interface for write backs and prioritizes write backs over regular writes.
- __mem_adr〈15..0〉:__ Memory address that data is being written back to or being read from.

### IE Logic

The following goes into detail with how the Instruction Execute handles operations on the op_code harness. For more detail see the actual .VHD files.

#### How IE Handles Format A

- __NOP:__  _No Operation_
- __ADD:__ _ADD ra,rb,rc: Adds rb to rc and stores result in ra_
- __SUB:__ _SUB ra,rb,rc: Subtracts rb from rc and stores result in ra_
- __MUL:__ _MUL ra,rb,rc: Multiplies rb and rc, stores result in ra_
- __NAND:__ _NAND ra,rb,rc: rb NAND rc store result in ra_ 
- __SHL:__ _SHL ra#n: Shift ra left by n_
- __SHR:__ _SHL ra#n: Shift ra right by n_
- __TEST:__ _TEST ra: Tests ra to see if the value is non-zero and non-negative_
- __OUT:__ _OUT ra: Outputs the contents of ra on the output harness_
- __IN:__ _IN ra: Loads the contents of the in signal harness into ra_

NOP instruction zeros the all IE outputs.

ADD, SUB, MUL and NAND instructions are forwarded on the op_code harness to the ALU. It reads data from rd_data1 nad rd_data2 and maps it to the ALU inputs alu_in1 and alu_in2.

SHL and SHR instructions are forwarded on the op_code harness to the ALU. It reads data on rd_data1 and maps it ALU input alu_in1. It also reads data on cl and maps the contents onto alu_in2.

TEST and OUT instructions are forwarded on the op_code harness to the ALU. It takes the contents of rd_data1 and maps it to ALU input alu_in1.

IN instruction is forwarded on the op_code harness to later stages. The IE then sets all other outputs as if it was a NOP.

#### How IE Handles Format B

- __BRR:__ BRR +disp.l: Increment the program counter by the amount specified by disp.l
- __BRR.N:__ BRR.N +disp.l: Increment the program counter by the amount specified by disp.l if the last instruction resulted in the negative flag of the ALU == TRUE.
- __BRR.Z:__ BRR.Z +disp.l: Increment the program counter by the amount specified by disp.l*2 if the last instruction resulted in the zero flag of the ALU == TRUE.
- __BR:__ BR ra+disp.s: Add displ*2 to the contents of ra, increment the program counter by the result.
- __BR.N:__ BR.N ra+disp.s: If the last instruction resulted in the negative flag of the ALU == TRUE, add (disp.s*2) to the contents of ra and increment the program counter by the result.
- __BR.Z:__ BR.Z ra+disp.s: If the last instruction resulted in the zero flag of the ALU == TRUE, add (disp.s*2) to the contents of ra and increment the program counter by the result.
- __BR.SUB:__ BR.SUB ra+disp.s: Saves (PC value + 1) into r7 the adds the contents of ra and disp.s and increments the program counter by that amount.
- __RETURN:__ Copies the contents of r7 into the PC.

BRR instruction is read and forwarded on the op_code harness and the IE NOPs all other outputs.

BRR.N, BRR.Z, BR, BR.N, BR.Z and BR.SUB instructions are forward on the op_code harness. The IE then takes the contents of rd_data1 and cl and maps them to the alu_in1 and alu_in2 harnesses respectively.

RETURN instruction is read and forwarded on the op_code harness. The IE maps the value of rd_data1 to alu_in1 harness.

#### How IE Handles Format L

- __LOAD:__  LOAD r[dst], r[src]: Load the contents of memory specified by the memory address in r[src] and store into r[dst]
- __STORE:__ STORE r[dst], r[src]: Store the contents of r[src] into a memory location specified by r[dst]
- __LOADIMM:__ LOADIMM.upper #n: Load a value specified by the 8 least significant bits of the instruction into either the most significant bits or least significant bits (specified by bit 8 of the instruction) into r7
- __MOV:__ MOV r[dst], r[src]: Copy the contents of r[src] into r[dst]

LOAD instruction is forwarded on the op_code harness to later stages. IE reads the contents of rd_data1 and maps the data to its alu_in1 harness. This data is then forwarded through the ALU to later stages.

STORE instruction is forwarded on the op_code harness to later stages. IE reads the contents of rd_data1 and maps the data to its alu_in1 harness. IE reads the contents of rd_data2 and maps this data to the mem_addr harness.

MOV instruction are forwarded on the op_code harness to later stages. The IE then takes the contents of rd_data1 and forwards it on alu_in1.

LOADIMM instruction is read and forwarded on the op_code harness to later stages. The IE takes the contents of rd_data1 and maps it to alu_in1. It also takes the contents of cl and maps it to alu_in2.

## Arithmetic Logic Unit (ALU)

TODO: Insert image

### ALU Description

The ALU is a PCU located after the Register File PCU. The ALU together with the Register File and RAM make up the Instruction Execute pipeline stage. The ALU is responsible for mostly every instruction and does the bulk of the instruction execution. The ALU takes data in on its three inputs and performs arithmetic or logic operations on this data based off the Operation Code specified by the op_code harness. The ALU has two outputs for data. The first output is the primary output and is used for every instruction. The secondary output is just for overflows and is currently ignored by later stages.

### ALU Inputs

- __clk:__ System clock from external source.
- __rst:__ System reset signal. All outputs are set to 0 for one clock cycle. No data is retained inside ALU.

#### ALU Specific Inputs

- __cl〈15..0〉:__ Operand supplied directly from the IF/ID stage. This is required for instructions were the data is not coming from the register file.
- __br_base〈15..0〉:__ Harness from the IF/ID stage that contains the current value of the PC. This harness is used only for Format B instructions.
- __in_op_code〈6..0〉:__ Harness containing the Operational Code for the current instruction. ALU uses this signal to determine its mode of operation. Harness is mapped to output out_op_code so the instruction is passed through the stage successfully.
- __in_data1〈15..0〉:__ Primary data input for the ALU. This is mapped directly to the RF read_data1 output.
- __in_data2〈15..0〉:__ Secondary data input for the ALU. This is mapped directly to the RF read_data2 output.

#### ALU Passed Through Inputs

- __in_wb_index〈2..0〉:__ Harness containing the index for write-back instructions. This is not used by the ALU and its mapped to the output out_wb_index.

### ALU Outputs

#### ALU Specific Outputs

- __out_data1〈15..0〉:__ Primary data output of the ALU. This output is used for all ALU operations.
- __out_data2〈15..0〉:__ Secondary data output of the ALU. This output is only used for overflow operations such as multiply. It is ignored by later stages.
- __out_mem_addr〈15..0〉:__ Output containing a memory address for a LOAD or STORE operation. For LOAD and STORE operations the contents of this harness is mapped from the in_data1 harness (RF read_data1).

#### ALU Passed Through Outputs

- __out_op_code〈6..0〉:__ Output harness containing the contents of in_op_code.
- __out_wb_index〈15..0〉:__ Output harness containing the contents of in_wb_index.

### ALU Logic

#### How ALU Handles Format A

- __NOP:__  No Operation
- __ADD:__ ADD ra,rb,rc: Adds rb to rc and stores result in ra
- __SUB:__ SUB ra,rb,rc: Subtracts rb from rc and stores result in ra
- __MUL:__ MUL ra,rb,rc: Multiplies rb and rc, stores result in ra
- __NAND:__ NAND ra,rb,rc: rb NAND rc store result in ra
- __SHL:__ SHL ra#n: Shift ra left by n
- __SHR:__ SHL ra#n: Shift ra right by n
- __TEST:__ TEST ra: Tests ra to see if the value is non-zero and non-negative
- __OUT:__ OUT ra: Outputs the contents of ra on the output harness
- __IN:__ IN ra: Loads the contents of the in signal harness into ra

NOP instruction is handled by setting all outputs to 0.

ADD, SUB, MUL and NAND instructions are handled by performing that operation on the contents of its two input, in_data1 and in_data2. The results is placed on out_data1 and if an overflow occurs the upper bits are placed on out_data2. It is important to note that in_data1 is the primary input so an operation like SUB will minus in_data2 from in_data1.

SHL and SHR instructions are handled by shifting the contents of in_data1 by the contents of cl. The output is made available on out_data1. The shift direction is specified by the opcode.

TEST instruction takes the contents of in_data1 and performs the test operation on it. The result of the TEST operation sets the internal flags z_flag and n_flag. These flags are latched meaning that they stay the same even if a NOP happens and can be used for later calculation of Format B instructions.

OUT and IN instructions passed through on the out_op_code harness and the contents of in_data1 is mapped to out_data1. All other outputs are set to 0.

#### How ALU Handles Format B

- __BRR:__ BRR +disp.l: Increment the program counter by the amount specified by disp.l
- __BRR.N:__ BRR.N +disp.l: Increment the program counter by the amount specified by disp.l if the last instruction resulted in the negative flag of the ALU == TRUE.
- __BRR.Z:__ BRR.Z +disp.l: Increment the program counter by the amount specified by disp.l*2 if the last instruction resulted in the zero flag of the ALU == TRUE.
- __BR:__ BR ra+disp.s: Add displ*2 to the contents of ra, increment the program counter by the result.
- __BR.N:__ BR.N ra+disp.s: If the last instruction resulted in the negative flag of the ALU == TRUE, add (disp.s*2) to the contents of ra and increment the program counter by the result.
- __BR.Z:__ BR.Z ra+disp.s: If the last instruction resulted in the zero flag of the ALU == TRUE, add (disp.s*2) to the contents of ra and increment the program counter by the result.
- __BR.SUB:__ BR.SUB ra+disp.s: Saves (PC value + 1) into r7 the adds the contents of ra and disp.s and increments the program counter by that amount.
- __RETURN:__ Copies the contents of r7 into the PC.

BRR instruction is handled by adding the contents of br_base to cl and making the result available on out_data1.

BRR.N and BRR.Z instructions are handled exactly the same as BRR except the contents of out_data1 is only set if the respective flags evaluate to TRUE. If the flag(s) are not TRUE, the output is set to br_base + 2, so the PC is not incremented by any amount and kept at a nominal value.

BR instruction is handled by  adding the contents of br_base to in_data1 and making the result available on out_data1.

BR.N and BR.Z instructions are handled exactly the same as BR except the contents of out_data1 is only set if the respective flags evaluate to TRUE. If the flag(s) are not TRUE, the output is set to cl + 2 so the PC is not incremented by any amount and kept at a nominal value.

BRSUB 

#### How ALU Handles Format L

## RAM

TODO: Insert image

### RAM Description

The Random Access Memory is a PCU located immediately after the ALU. This is to speed up the execution time of Format L instructions. RAM is used for storing data relevant to the current application the processor is running. RAM is volatile meaning that is reset during a power failure. RAM spends most of its time passing through the results of the ALU until is sees a LOAD or STORE instruction on the op_code harness. The RAM then either reads the contents of memory specified by the LOAD and makes it available for later stages to write back into the RF or it writes the contents on its input data harness into a memory location specified by the STORE instruction.

### RAM Inputs

- __clk:__ System clock from external source.
- __rst:__ System reset signal. When signal is high all RAM contents are set to 0.

#### RAM Specific Inputs

- __in_op_code〈6..0〉:__ Harness containing the Operational Code for the current instruction. RAM uses this signal to determine its mode of operation. Harness is mapped to output out_op_code so the instruction is passed through the stage successfully.
- __mem_addr〈15..0〉:__ Address relevant for current RAM operation. Both LOAD and STORE instructions share this harness.
- __in_data1〈15..0〉:__ Data from ALU to be written into RAM via the STORE command. The harness is mapped to output out_data so the data for the current instruction is passed through the stage successfully.

#### Passed Through Inputs

- __in_data2〈15..0〉:__ Data from the secondary ALU output. This is only used for multiply instructions which contains the lower 16 bits. This signal is not mapped to anything and is terminated here in RAM.
- __in_wb_index〈2..0〉:__ RF index for write-back instructions. This harness is passed through for later stages.

### RAM Outputs

#### RAM Specific Outputs

- __out_data〈15..0〉:__ The data output from RAM. This contains either the result of a LOAD command or contains the passed through contents from in_data1.

#### RAM Passed Through Outputs

- __out_op_code〈6..0〉:__ Operation Code for the current instruction. Harness contains whatever contents is seen on in_op_code.
- __out_wb_index〈2..0〉:__ RF index for write-back instructions. This harness contains whatever contents is seen on in_wb_index

### Memory Logic

#### How RAM Handles Format A

- __NOP:__ No Operation

NOP is handled by setting all outputs to 0.

Any other op_code seen on in_op_code that contains a Format A instruction is passed through the RAM stage. This means that out_data, out_op_code and out_wb_index are mapped directly to in_data1, in_op_code and in_wb_index respectively.

#### How RAM Handles Format B

Any op_code seen on in_op_code that contains a Format B instruction is passed through the RAM stage. This means that out_data, out_op_code and out_wb_index are mapped directly to in_data1, in_op_code and in_wb_index respectively.

#### How RAM Handles Format L

- __LOAD:__  LOAD r[dst], r[src]: Load the contents of RAM specified by the RAM address in r[src] and store into r[dst]
- __STORE:__ STORE r[dst], r[src]: Store the contents of r[src] into a RAM location specified by r[dst]
- __LOADIMM:__ LOADIMM.upper #n: Load a value specified by the 8 least significant bits of the instruction into either the most significant bits or least significant bits (specified by bit 8 of the instruction) into r7
- __MOV:__ MOV r[dst], r[src]: Copy the contents of r[src] into r[dst]

__LOAD__ instruction is seen on the in_op_code harness and the contents of RAM specified by mem_addr is placed on the output out_data. The remaining outputs are set to their passed-through input values.

__STORE__ instruction is seen on the in_op_code harness and the contents of in_data1 is written into RAM at location specified by mem_addr. The outputs out_data and out_wb_index are set to 0. The output out_op_code is set to the passed through value of in_op_code.

## Data Control (DC)

![Data Controller](DC.png)

### DC Description

The Data Control Unit is a POU component that controls the flow of data as it nears the end of the pipeline. The DC is forwarded the op_code harness and has logic to read this harness and execute relevant instructions. All non-relevant instructions are terminated here and the DC reacts as a NOP.

### DC Inputs

- __op_code〈6..0〉:__
- __wb_index〈2..0〉:__ RF address that data is being written back to. It is important to note that the RF address has a separate write interface for write backs and prioritizes write backs over regular writes.
- __mem_adr〈15..0〉:__ Memory address that data is being written back to or being read from.
- __out1〈15..0〉:__ The main output of the ALU. All ALU operations result in out1 harness containing the result, except for the multiplication.
- __out2〈15..0〉:__ The secondary output of the ALU for extended operations. Multiplication is currently the only operation that produces valid data on the out2 signal harness. The current design is for the DC to ignore all outputs on this.

### DC Outputs

#### Write Back Interface Output

- __wb_index〈2..0〉:__ RF register that data is to be written back to
- __wb_data〈15..0〉:__ Data that is to be written back into RF
- __wb_en:__ Enables the RF to write data on wb_data into a register specified by rf_adr.
- __br_en:__ Enables the IF/ID to add the contents of br_pc to the current value of the PC
- __br_pc〈15..0〉:__Data harness for the value to increment the PC by. Note that the contents of this harness is always added to the current value of the PC.

#### Memory Output

- __mem_adr〈15..0〉:__ Memory address that the current memory operation needs to access
- __mem_wr_data〈15..0〉:__ Data on this signal harness is to be written into memory
- __mem_mode〈1..0〉:__ Specifies mode of operation for the memory.
  - __00:__ _NOP command is issued, memory does nothing_
  - __01:__ _READ command, memory will output the contents stored at an address specified by mem_addr onto the wb_data harness_
  - __10:__ _WRITE command, data on mem_wr_data harness is written into memory at a location specified by mem_addr harness_
  - __11:__ _NON-specified command, memory treats it as a NOP.

#### External Output

- __out〈15..0〉:__ _Data output of the processor. This signal harness is brought outside of the processor and is made available on the processor i/o._

### DC Logic

Detailed descriptions of how DC handles each instruction that is seen on the op_code harness. If the instruction seen on the op_code harness is not recognized or no instruction at all is see, the DC will NOP and all outputs are set to logic 0.

#### How DC Handles Format A

- __NOP:__  _No Operation_
- __ADD:__ _ADD ra,rb,rc: Adds rb to rc and stores result in ra_
- __SUB:__ _SUB ra,rb,rc: Subtracts rb from rc and stores result in ra_
- __MUL:__ _MUL ra,rb,rc: Multiplies rb and rc, stores result in ra_
- __NAND:__ _NAND ra,rb,rc: rb NAND rc store result in ra_
- __SHL:__ _SHL ra#n: Shift ra left by n_
- __SHR:__ _SHL ra#n: Shift ra right by n_
- __TEST:__ _TEST ra: Tests ra to see if the value is non-zero and non-negative_
- __OUT:__ _OUT ra: Outputs the contents of ra on the output harness_
- __IN:__ _IN ra: Loads the contents of the in signal harness into ra_

NOP is a No Operation command, all outputs are set to zero.

ADD, SUB, NAND, SHL and SHR instructions have results that need to be written back into the RF. When the DC sees these instructions on the op_code harness, it remaps data on out1 to wb_data in the write back interface. The DC also sets the wb_en high to inform the RF that the data on the write back interface is relevant. The rest of the outputs are set as a NOP.

MUL instruction can result in the second output of the ALU being used. Currently only the least significant bits are being used from out1. The contents of out2 are being ignored entirely. Besides that, the MUL instruction is handled exactly like a ADD, SUB, NAND or SHL/R instruction.

OUT instruction is read on the op_code harness and the contents of out1 are mapped to the output interface. The rest of the outputs are set as a NOP.

TEST and IN instructions are ignored and handled as a NOP.

#### How DC Handles Format B

- __BRR:__ BRR +disp.l: Increment the program counter by the amount specified by disp.l
- __BRR.N:__ BRR.N +disp.l: Increment the program counter by the amount specified by disp.l if the last instruction resulted in the negative flag of the ALU == TRUE.
- __BRR.Z:__ BRR.Z +disp.l: Increment the program counter by the amount specified by disp.l*2 if the last instruction resulted in the zero flag of the ALU == TRUE.
- __BR:__ BR ra+disp.s: Add displ*2 to the contents of ra, increment the program counter by the result.
- __BR.N:__ BR.N ra+disp.s: If the last instruction resulted in the negative flag of the ALU == TRUE, add (disp.s*2) to the contents of ra and increment the program counter by the result.
- __BR.Z:__ BR.Z ra+disp.s: If the last instruction resulted in the zero flag of the ALU == TRUE, add (disp.s*2) to the contents of ra and increment the program counter by the result.
- __BR.SUB:__ BR.SUB ra+disp.s: Saves (PC value + 1) into r7 the adds the contents of ra and disp.s and increments the program counter by that amount.
- __RETURN:__ Copies the contents of r7 into the PC.

Branch instructions that are evaluated depending on the status of the z and n flags from the ALU are done so for the previous state of those flags. Most outputs are reset to zero if their status does not change but the instruction does to help prevent data hazards. The z and n flags are exceptions to this rule as they are needed to for branching.

BRR instruction is read and handled as a NOP.

BRR.N and BR.N instructions are read and the DC evaluates the status of the n_flag. If the n_flag is high, the DC maps the contents of out1 to br_pc and forces pc_en high. If the n_flag is low, the DC leaves pc_en and pc_br as all zeros.

BRR.Z and BR.Z instructions are read and the DC evaluates the status of the z_flag. If the z_flag is high, the DC maps the contents of out1 to br_pc and forces the pc_en high. If the n_flag is low, the DC leaves pc_en and pc_br as all zeros.

BR, BR.SUB and RETURN instructions are read and the DC takes the contents of out1 and maps it to the br_pc harness. It then forces br_en high.

#### How DC Handles Format L

- __LOAD:__  LOAD r[dst], r[src]: Load the contents of memory specified by the memory address in r[src] and store into r[dst]
- __STORE:__ STORE r[dst], r[src]: Store the contents of r[src] into a memory location specified by r[dst]
- __LOADIMM:__ LOADIMM.upper #n: Load a value specified by the 8 least significant bits of the instruction into either the most significant bits or least significant bits (specified by bit 8 of the instruction) into r7
- __MOV:__ MOV r[dst], r[src]: Copy the contents of r[src] into r[dst]

When a LOAD instruction is seen on the op_code harness, the DC forwards the contents of mem_addr to memory and sets mem_mode harness to `01` for a READ command. The contents of wb_index are placed in a one clock cycle buffer. DC waits one clock cycle then sets wb_en high and forwards the buffered value of wb_index to the write-back interface. This is to allow time for memory to output the correct data into the DC latch. Note that CCU prevents data hazards from occuring due to the one extra clock cycle by issuing a NOP into the pipeline immediately after a LOAD instruction has entered.

STORE instruction is seen on the op_code harness and DC forwards mem_addr to mem_addr and maps the contents of out1 to mem_wr_data. It then sets mem_mode harness to `10` to issue a WRITE command.

LOADIMM instruction is read on the op_code harness and forwarded on to later stages via the op_code harness. The DC takes the input out1 from the ALU and maps it to wb_data. It also takes the contents of wb_index and maps it the output wb_index. It then issues wb_en high for the updated contents of R7 to be written back into R7.

MOV instruction is read on the op_code harness and the contents of out1 are mapped to wb_data. The DC then takes maps the contents of wb_index on the input to wb_index on the output. It then forces wb_index high.

## Latches

### Latch Description

Latches have inputs harnesses and outputs harnesses. Whatever bits are made available on a latch input harness is made available on that latch output harness the next clock cycle. Inputs and outputs are active on the rising clock edge. The latch itself has a single input signal from the CCU to issue a NOP command.

The NOP command issues a logic low on all output signals. The NOP command issued and can be issued anywhere after the first rising edge and before the next rising edge. This is important to note because the NOP command is implemented next clock cycle and the data is never in the latch for more than one clock cycle unless a NOP command is issued. The data that is in the latch during a NOP command stays in the latch until the NOP command is removed. This allows the latch to save the current state of the pipeline during the NOP. Please note that issuing a NOP command to only the latch will cause data loss in the pipeline as the previous stage will continue to generate data that is being ignored by the latch.

A latch has a generic design, but the number of signals that it can latch change depending on the latches location in the pipeline. The following is a list of latches and the signal names they are latching.

### Latch Types

#### Instruction Execute/Instruction Decode (IF/ID) Latch

This latch contains information from the IF/ID stage and is effectively the gateway to the pipeline. It is important to monitor to prevent new instructions from access frozen registers in the RF.

- op_code〈6..0〉
- wb_index〈2..0〉
- rd_index1〈2..0〉 -> mapped to CCU
- rd_index2〈2..0〉 -> mapped to CCU
- wr_index〈2..0〉  -> mapped to CCU
- wr_en〈1〉        -> mapped to CCU
- cl〈15..0
- wr_en〈1〉

#### Instruction Execute (IE) Latch

This latch contains information from the IE stage. This latch is used to keep continuity in the controller logic and allow it properly queue the instructions in the pipeline.

- op_code〈2..0〉
- wb_index〈2..0〉 -> mapped to CCU
- mem_adr〈15..0〉
- alu_in1〈15..0〉
- alu_in2〈15..0

#### Data Control (DC) Latch

This latch contains information from the DC stage. Is monitored to prevent data hazards from occurring by reading/writing over frozen registers and/or memory locations.

- wb_index〈2..0〉 -> mapped to CCU
- wb_en〈1〉       ~~-> mapped to CCU~~ not needed of each stage outputs NOP if that output has not been updated
- wb_data〈15..0〉
- mem_adr〈15..0〉
- mem_mode〈1..0〉
- mem_wr_data〈15..0〉
- output〈15..0〉

## Central Control Unit (CCU)

TODO: Insert CU image

### CCU Description

The Central Control Unit prevents data hazards from occurring in the pipeline and does so while reducing the complexity of each individual pipeline component. The CCU monitors the input of the pipeline and checks each instruction that is entering. It places the instruction in a queue type data structure that allows for the CCU to monitor priority of each instruction and the current state of each instruction the in the pipeline. If an instruction enters the pipeline that writes or results in a write back to the RF, that RF index is marked as frozen and placed in a queue.

The CCU is also responsible for managing Format B instructions.

### Overview of Data Hazards

#### Write-back Overwrite

__Description:__
Writing new data into a register before an instruction in the pipeline executes a write-back to it. The new data is written immediately after the instruction is decoded and the data from the write-back overwrites the new data later on. This causes a hazard as later access of the register will result in contents of the write-back and not contents of the more recent new data.

__Solution:__
When an instruction with a wr_en == 1 is placed in the IF/ID latch, CCU compares the value of wr_index to the value of wb_index in IE and DC latches. If there is a match it NOPs the IF/ID and IF/ID latch.

#### Memory-read Overwrite

__Description:__
Writing new data into a register before an instruction in the pipeline can execute the LOAD command into the same register. The new data is written immediately after the instruction is decoded and the data from memory overwrites the new data later on. This causes a hazard as later access of the register will result in contents of the LOAD and not the contents of the more recent new data.

__Solution:__
The solution is handled by Write-back Overwrite prevention.

#### Write-back Interface Busy

__Description:__
DC outputting a write-back over the Write-back Interface one instruction after it issued a LOAD command to memory. Memory needs one clock to output the data for the LOAD command and any command immediately afterwards that also uses the Write-back Interface will cause a collision.

__Solution:__
When an instruction with op_code == LOAD enters the IF/ID Latch, CCU issues a NOP to the IF/ID and IF/ID latch.

#### Read Before Write-back

__Description:__
Reading data from a register before an instruction in the pipeline executes a write-back to it. The data is read immediately after decode and contains older data when it should be data from the write-back. This causes a data hazard as the instruction issuing the read is using outdated data.

__Solution:__
When the contents of rd_index1 or rd_index2 in the IF/ID latch equals a non-zero value, CCU must check the value of rd_index1 and rd_index2 against the value of wb_index at the IE latch and DC latch. If either rd_data1 or rd_data2 value matches the contents of wb_index at any latch, the If/ID and IF/ID latch is issued a NOP.

#### Read Before Load

__Description:__
Reading data from a register before an instruction in the pipeline executes a LOAD operation to that register. The data is read immediately after decode and contains older data when it should be data from the LOAD operation. This causes a data hazard as the instruction issuing the read is using outdated data.

__Solution:__
The solution is handled by Read Before Write-back prevention.

### Overview of Branching

The current status of branching is to simply issues NOPs until the branches have been resolved. The pipeline is relatively short and is already optimized for quickly handling branches so adding additional logic to take a branch and then possibly roll back later on is not really beneficial.

Due to this design choice, the CCU does not play much of a roll in preventing data hazards due to branching. All it does is monitor the IF/ID latch and check if a branch instruction has been placed in the latch and NOP until the the branch instruction is removed from the pipeline.

#### Pipeline Continuation Before Branch is Resolved

__Description:__
Pipeline continues processing instructions before a branch instruction is fulled resolved. This could result in a data hazard occurring when a later instruction is processed before the branch instruction can be resolved and update the PC to the correct value. The branch might resolve and the later instruction needs to be executed as normal, or the branch could resolve and the later instruction is no longer the next instruction and therefore was invalid.

__Solution:__
If the contents of the op_code in either the IF/ID latch or IE latch is equal to the opcode of any Format B instruction except BRR, the CCU NOPS the IF/ID and IF/ID latch.

### CCU Inputs

CCU inputs are specific contents from each latch.

#### IF/IF Latch

- __op_code〈6..0〉:__ Current operation that the IF/ID is sending into the pipeline. The CCU needs this information to prevent immediate writes/reads to the RF on frozen registers
- ~~__wb_index〈2..0〉:__~~
- __rd_index1〈2..0〉:__ The first read index that the current operation is reading from. CCU needs to check if this is a frozen register.
- __rd_index2〈2..0〉:__ The second read index that the current operation is reading from. CCU needs to check if this is a frozen register.
- __wr_index〈2..0〉:__
- __wr_en〈1〉:__

#### IE Latch

#### DC Latch

### CCU Outputs

- __IFIF_NOP〈1〉:__ No Operation command that is mapped to both IF/ID NOP input and IF/ID Latch NOP input

### CCU Logic

__Instruction Fetch/Instruction Decode (IF/ID) Latch:__ This latch gives us information about what instruction has entered the pipeline. The contents of this latch that is relevant is as follows:

* op_code〈6..0〉: What the current operation is for that instruction. This allows the controller to identify data hazards.
* wb_index〈2..0〉: What register will this instruction be writing back to. This notifies if it needs ot freeze a register
* rd_index1〈2..0〉/read_index2〈2..0〉: What register this instruction is reading from and is that register frozen. Controller does care if index1 or index2 is reading a frozen register, it only cares if that instruction will be attempting to read from a frozen register. This can be done as a read_index1 OR read_index2 => logic to see if the value is frozen.
* wr_index〈2..0〉: What register this instruction is writing to now and if it needs to freeze that register for future use.
* wr_en〈1〉: If the instruction is actually writing. This allows Controller to only care about wr_index when the wr_en flag is active. Simple if wr_en == 1 then execute wr_index logic.

Controller does not care about the remaining contents of the latch, for clarification they are presented as follows:

* cl〈3..0〉: This is only relevant for the ALU during shift operations.
* wr_data〈15..0〉:This is only relevant to the Register File, the actual contents of the data do not matter.

__Instruction Execute (IE) Latch:__ This latch  gives us information about what instruction has been executed and what type of result it will produce.

* alu_mode〈2..0〉: What operation the ALU will be performing. This is important to note as the ALU might take more than one clock cycle to execute operation and output the result.
* wb_index〈2..0〉: What register will this result be written back to. This notifies if it needs to freeze a register.
* wb_op〈1..0〉: This contains information regarding the write back operation. A 01 or 10 means the current instruction will not be creating any write back data. 00 means the current instruction result will be writing back to Register File and 11 means the instruction will be writing back to Memory.
* mem_rd_adr〈15..0〉: Ig the current instruction is writing back to Memory, this latch elements allows Controller to identify data hazards with that location in Memory.

Controller does not care about the remaining  contents of the latch, for clarification they are presented as follows:

* alu_in1〈15..0〉: The first operand for the ALU. Controller does not care about the value, only th operation.
* alu_in2〈15..0〉: The second operand, the value also does not matter.
* wr_data〈15..0〉: The data being written back, this value does not matter.

__Data Controller (DC) Latch:__ This latch gives us information about the result of the instruction and where the data from that instruction is going.

* wb_en〈1〉: This alerts the Register File that the data on the bus needs to be written back. It also alerts Controller about write back operation to the Register File.
* wb_index〈2..0〉: This is the Register File index for the write back data. Controller needs to know so it can check for data hazards.
* mem_wr_en〈1〉: Alerts Memory that data on mem_wr_adr and mem_wr_data is relevant and needs to be written. Controller needs to know just as Memory does, otherwise it doesn't know if the contents of mem_wr_adr is relevant and a current operation.
* mem_wr_adr〈15..0〉: This contains the memory address that the DC is writing data to.
* mem_rd_en〈1〉: Alerts Memory that the data on mem_rd_adr is relevant and Memory contents of mem_rd_adr needs to be written to mem_rd_data. Controller needs to know just as the Memory does, otherwise it doesn't know if the contents of mem_rd_adr is relevant and a current operation.
* mem_rd_adr〈15..0〉: This contains the memory address that the IE is about to read from. Note that mem_rd_adr is present at the DC Latch but it is forwarded directly from the IE and DC does not see this interface.


* wb_data〈15..0〉: The data being written back onto the register. The controller does not care about the value of this data.
* mem_rd_data〈15..0〉: The data being written into the memory. the controller does not care about this memory.

## Write Back Bus (WBB)

The write back interface allows both the Data Controller and the Memory to write directly back into the Register File. The data on the WBB can be from either DC or Memory and it requires Controller to monitor DC Latch and insert NOP commands into the memory to prevent data hazards from happening on the WBB. The WBB contains the following:

* wb_en〈1〉: Alerts the Register File if the data on the wb_data and wb_index lines are relevant to the current operation.
* wb_index〈2..0〉: Contains the index for the data to be written to.
* wb_data〈15..0〉: Contains the data being written into the Register File.

# Software

## Instruction Set