--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   16:22:59 01/24/2018
-- Design Name:   
-- Module Name:   C:/Users/adoknjas/Documents/CENG450/Count_Mem_VHDL/count_mem_tb_vhdl.vhd
-- Project Name:  Count_Mem_VHDL
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: count_mem
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY count_mem_tb_vhdl IS
END count_mem_tb_vhdl;
 
ARCHITECTURE behavior OF count_mem_tb_vhdl IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT count_mem
    PORT(
         CLK : IN  std_logic;
         RESET : IN  std_logic;
         CE : IN  std_logic;
         OUT_MEM : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CLK : std_logic := '0';
   signal RESET : std_logic := '0';
   signal CE : std_logic := '0';

 	--Outputs
   signal OUT_MEM : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant clk_period : time := 0.2 us;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: count_mem PORT MAP (
          CLK => CLK,
          RESET => RESET,
          CE => CE,
          OUT_MEM => OUT_MEM
        );

   -- Clock process definitions
   clk_process :process
   begin
		CLK <= '0';
		wait for clk_period/2;
		CLK <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      RESET <= '1';
		CE <= '1';
		wait for 600 ns;
		reset <= '0';

      -- insert stimulus here 

      wait;
   end process;

END;
