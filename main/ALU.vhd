LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_SIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;


entity alu is
port(
	-- System
	rst: 			in std_logic;
	clk: 			in std_logic;
	
	--Inputs
	cl: 			in std_logic_vector(15 downto 0);
	br_base: 		in std_logic_vector(15 downto 0);
	in_op_code: 	in std_logic_vector(6 downto 0);
	in_data1: 		in std_logic_vector(15 downto 0);
	in_data2: 		in std_logic_vector(15 downto 0);

	-- Passed through inputs
	in_wb_index:	in std_logic_vector(2 downto 0);
	
	-- ALU outputs
	out_op_code:	out std_logic_vector(6 downto 0);
	out_data1:		out std_logic_vector(15 downto 0);
	out_data2:		out std_logic_vector(15 downto 0);
	out_mem_addr:	out std_logic_vector(15 downto 0);

	-- ALU passed through outputs
	out_wb_index:	out std_logic_vector(2 downto 0)

	);
		
end alu;

architecture Behavioral of alu is

-- Results
signal result_add: 		std_logic_vector (15 downto 0) := (others => '0');
signal result_sub: 		std_logic_vector (15 downto 0) := (others => '0');
signal result_mult: 	std_logic_vector (31 downto 0) := (others => '0');
signal result_nand: 	std_logic_vector (15 downto 0) := (others => '0');
signal result_shl: 		std_logic_vector (15 downto 0) := (others => '0');
signal result_shr: 		std_logic_vector (15 downto 0) := (others => '0');
signal result_brr: 		std_logic_vector (15 downto 0) := (others => '0');
signal result_brr_fail: std_logic_vector (15 downto 0) := (others => '0');
signal result_br: 		std_logic_vector (15 downto 0) := (others => '0');
signal result_br_fail: 	std_logic_vector (15 downto 0) := (others => '0');
signal result_limm_up: 	std_logic_vector (15 downto 0) := (others => '0');
signal result_limm_low: std_logic_vector (15 downto 0) := (others => '0');

-- Z and N flag states
signal z_flag  	: std_logic := '0';
signal n_flag  	: std_logic := '0';

-- Format A instructions
constant NOP    	: integer := 0;
constant ADD    	: integer := 1;
constant SUB    	: integer := 2;
constant MUL    	: integer := 3;
constant NAAND  	: integer := 4;
constant SHL    	: integer := 5;
constant SHR    	: integer := 6;
constant TEST   	: integer := 7;

-- Format B Instructions
constant BRR 		: integer := 64;
constant BRRN 		: integer := 65;
constant BRRZ 		: integer := 66;
constant BR 		: integer := 67;
constant BRN 		: integer := 68;
constant BRZ 		: integer := 69;
constant BRSUB 	: integer := 70;
constant BRETURN 	: integer := 71;

-- Format L Instructions
constant LOAD	 	: integer := 16;
constant STORE  	: integer := 17;
constant LOADIMM	: integer := 18;
constant MOV		: integer := 19;

begin

process(clk)
begin
	if rising_edge(clk) then
		
		if(rst = '1') then
			z_flag 		 <= '0';
			n_flag 		 <= '0';
			out_data1 		 <= (others => '0');
			out_data2 <= (others => '0');
			out_wb_index <= (others => '0');
			out_mem_addr <= (others => '0');
			out_op_code		 <= (others => '0');
			--result_mult  <= (others => '0');
		else
			out_wb_index <= in_wb_index;
			out_op_code		 <= in_op_code;
			out_mem_addr <= (others => '0');
			case to_integer(unsigned(in_op_code(6 downto 0))) is
				when NOP =>
					out_data1 		 <= (others => '0');
					out_data2 <= (others => '0');
					out_wb_index <= (others => '0');
					out_mem_addr <= (others => '0');
					--result_mult  <= (others => '0');
				
				when ADD =>
					out_data1 <= result_add;
				
				when SUB =>
					out_data1 <= result_sub;
				
				when MUL =>
					out_data1 <= result_mult(15 downto 0);
					out_data2 <= result_mult(31 downto 16);
				
				when NAAND =>
					out_data1 <= result_nand; 
				
				when SHL =>
					out_data1 <= result_shl;
				
				when SHR =>
					out_data1 <= result_shr;
					
				when BRR =>
					out_data1 <= result_brr;				
					
				when BR =>
					out_data1 <= result_br;
				
				when BRRN =>
					if (n_flag = '1') then
						out_data1 <= result_brr;
					else
						out_data1 <= result_brr_fail;
					end if;
					
				when BRN =>
					if (n_flag = '1') then
						out_data1 <= result_br;
					else
						out_data1 <= result_br_fail;
					end if;
				
				when BRRZ =>
					if (z_flag = '1') then
						out_data1 <= result_brr;
					else
						out_data1 <= result_brr_fail;
					end if;
					
				when BRZ =>
					if (z_flag = '1') then
						out_data1 <= result_br;
					else
						out_data1 <= result_br_fail;
					end if;
					
				when BRSUB =>
					out_data1 <= cl;
								
				when LOADIMM =>
					if (cl(15) = '1') then
						out_data1 <= result_limm_up; 
					else
						out_data1 <= result_limm_low;
					end if;
					
				when STORE =>
					out_mem_addr <= in_data1;
					out_data1 <= in_data2;
					
				when LOAD =>
					out_mem_addr <=  in_data1;
					
				when TEST =>
					--check if less than 0
					if(signed(in_data1) < 0) then 		-- Less than 0
						n_flag <= '1';
						z_flag <= '0';
					elsif (signed(in_data1) = 0) then  -- equal to 0
						n_flag <= '0';
						z_flag <= '1';
					else									-- greater than 0
						n_flag <= '0';
						z_flag <= '0';
					end if;
				
				-- everything else just pass it on through
				when others =>					
					--Dump the input to out_data1 because we might want to use the input again
					out_data1 <= in_data1;
					out_data2 <= (others => '0');
			end case;
		end if;
	end if;
end process;

-- CONCURRENT PROCESSES
result_add  	 <= std_logic_vector(signed(in_data1) + signed(in_data2));
result_sub 		 <= std_logic_vector(signed(in_data1) - signed(in_data2));
result_mult 	 <= std_logic_vector(signed(in_data1) * signed(in_data2));
result_nand  	 <= std_logic_vector(signed(in_data1) nand signed(in_data2));
result_shl  	 <= std_logic_vector(shift_left(signed(in_data1), to_integer(unsigned(cl))));
result_shr  	 <= std_logic_vector(shift_right(signed(in_data1), to_integer(unsigned(cl))));
-- Branching
result_brr 		 <= std_logic_vector(unsigned(signed(br_base) + signed(cl)));
result_brr_fail <= std_logic_vector(unsigned(signed(br_base) + 2));
result_br    	 <= std_logic_vector(unsigned(signed(br_base) + signed(in_data1)));
result_br_fail  <= std_logic_vector(unsigned(signed(cl) + 2));
-- LOADIMM
result_limm_up  <= cl(7 downto 0) & in_data1(7 downto 0);
result_limm_low <= in_data1(15 downto 8) & cl(7 downto 0);
--END CONCURRENT PROCESSES

end Behavioral;

