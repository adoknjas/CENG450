
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;
use IEEE.STD_LOGIC_SIGNED.all;
USE IEEE.NUMERIC_STD.ALL;

entity register_file is
port(
--system ports
		rst 			: in std_logic;
		clk			: in std_logic;

		--read port
		rd_index1	: in std_logic_vector(2 downto 0); 
		rd_index2	: in std_logic_vector(2 downto 0);
		rd1_fwd		: in std_logic;
		rd2_fwd		: in std_logic;
		fwd_data1   : in std_logic_vector(15 downto 0);
		fwd_data2	: in std_logic_vector(15 downto 0);
		in_fwd_info : in std_logic_vector(1 downto 0);

		--write-in port
		wr_index		: in std_logic_vector(2 downto 0); 
		wr_data		: in std_logic_vector(15 downto 0); 
		wr_en			: in std_logic;

		--wrte_back bus port
		wb_index		: in std_logic_vector(2 downto 0); 
		wb_data		: in std_logic_vector(15 downto 0); 
		wb_en			: in std_logic;

		-- other ports just passing through
		in_opcode		: in std_logic_vector(6 downto 0); 
		in_wb_index		: in std_logic_vector(2 downto 0);
		in_cl				: in std_logic_vector(15 downto 0);
		in_br_base 	 	: in std_logic_vector (15 downto 0);

		-- readout ports
		rdo_index1	: out std_logic_vector(2 downto 0); 
		rdo_index2	: out std_logic_vector(2 downto 0);
		rd_data1		: out std_logic_vector(15 downto 0); 
		rd_data2		: out std_logic_vector(15 downto 0);
		out_fwd_info: out std_logic_vector(1 downto 0);

		-- other ports
		out_wb_index	: out std_logic_vector(2 downto 0);
		out_opcode		: out std_logic_vector(6 downto 0); 
		out_cl			: out std_logic_vector(15 downto 0);
		out_br_base 	: out std_logic_vector (15 downto 0)
);

end register_file;

architecture behavioural of register_file is

type reg_array is array (integer range 0 to 7) of std_logic_vector(15 downto 0);
signal reg_file : reg_array; 

begin

--write operation 
process(clk)
begin
   if rising_edge(clk) then 
		if(rst='1') then
			--Clear all registers
			for i in 0 to 7 loop
				reg_file(i)<= (others => '0');
			end loop;
			rdo_index1	<= (others => '0');
			rdo_index2	<= (others => '0');
			rd_data1	<= (others => '0');
			rd_data2	<= (others => '0');
			out_wb_index <= (others => '0');
			out_opcode <= (others => '0');
			out_cl <= (others => '0');
			out_br_base <= (others => '0');
			out_fwd_info <= (others => '0');
			
		else
			if (wb_en='1') then
				reg_file(to_integer(unsigned(wb_index))) <= wb_data;
			end if;

			if(wr_en='1') then
				reg_file(to_integer(unsigned(wr_index))) <= wr_data;
			end if;
			-- Read Outputs
			if (rd1_fwd = '1') then
				rd_data1 <= fwd_data1;
			else
				rd_data1 <=	reg_file(to_integer(unsigned(rd_index1)));
			end if;
			
			if (rd2_fwd = '1') then
				rd_data2 <= fwd_data2;
			else
				rd_data2 <=	reg_file(to_integer(unsigned(rd_index2)));
			end if;
			
			rdo_index1	<= rd_index1;
			rdo_index2	<= rd_index2;
			out_wb_index <= in_wb_index;
			out_opcode <= in_opcode;
			out_cl <= in_cl;
			out_br_base <= in_br_base;
			out_fwd_info <= in_fwd_info;

		end if;
   end if;
end process;



end behavioural;