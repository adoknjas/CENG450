
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity controller is
port(
	clk	  : in std_logic;
	rst 	  : in std_logic;
	inport  : in std_logic_vector(15 downto 0);
	outport : out std_logic_vector(15 downto 0)
	);
end controller;

architecture Behavioral of controller is

-- FETCH Signals
signal fd_pc_in 	  : std_logic_vector (6 downto 0)  := (others => '0');
signal fd_pc_out 	  : std_logic_vector (6 downto 0)  := (others => '0');
signal fd_data 	  : std_logic_vector (15 downto 0) := (others => '0');
-- END FETCH

-- REGFILE Signals
signal rf_rd_index1 : std_logic_vector (2 downto 0) := (others => '0');
signal rf_rd_index2 : std_logic_vector (2 downto 0) := (others => '0');
signal rf_fwd_data1  	: std_logic_vector (15 downto 0) := (others => '1');
signal rf_fwd_data2  	: std_logic_vector (15 downto 0) := (others => '1');
signal rf_wr_index  : std_logic_vector (2 downto 0) := (others => '0');
signal rf_wr_enable : std_logic := '0';
signal rf_forward   : std_logic_vector (1 downto 0)  := (others => '0');
signal rf_wr_data   : std_logic_vector (15 downto 0) := (others => '0');
signal rf_opcode    : std_logic_vector (6 downto 0) := (others => '0');
signal rf_wb_index  : std_logic_vector (2 downto 0) := (others => '0');
signal rf_cl        : std_logic_vector (15 downto 0) := (others => '0');
signal rf_br_base   : std_logic_vector (15 downto 0) := (others => '0');
-- END REGFILE

--ALU Signals
signal alu_opcode 	: std_logic_vector (6 downto 0) := (others => '0');
signal alu_index1   : std_logic_vector (2 downto 0) := (others => '0');
signal alu_index2   : std_logic_vector (2 downto 0) := (others => '0');
signal alu_in1 		: std_logic_vector (15 downto 0) := (others => '0');
signal alu_in2 		: std_logic_vector (15 downto 0) := (others => '0');
signal alu_fwd_info  : std_logic_vector (1 downto 0) := (others => '0');
signal alu_cl 			: std_logic_vector (15 downto 0) := (others => '0');
signal alu_br_base 	: std_logic_vector (15 downto 0) := (others => '0');
signal alu_wb_index 	: std_logic_vector (2 downto 0) := (others => '0');
--END ALU

--MEMORY Signals
signal mem_addr_in  	: std_logic_vector (15 downto 0) := (others => '0');
signal mem_data_in  	: std_logic_vector (15 downto 0) := (others => '0');
signal mem_opcode  	: std_logic_vector (6 downto 0) := (others => '0');
signal mem_upper  	: std_logic_vector (15 downto 0) := (others => '0');
signal mem_wb_index  : std_logic_vector (2 downto 0) := (others => '0');
--END MEMORY

-- DATA CONTROLLER Signals
signal dc_opcode	 	: std_logic_vector (6 downto 0)  := (others => '0');
signal dc_wb_index 	: std_logic_vector (2 downto 0)  := (others => '0');
signal dc_wb_data  	: std_logic_vector (15 downto 0) := (others => '0');


signal wbb_index 		: std_logic_vector (2 downto 0)  := (others => '0');
signal wbb_data  		: std_logic_vector (15 downto 0) := (others => '0');
signal wbb_en 			: std_logic := '0';
signal wbb_opcode		: std_logic_vector (6 downto 0)  := (others => '0');

signal br_en 			: std_logic := '0';
signal br_pc  			: std_logic_vector (15 downto 0) := (others => '0');

-- Forwarding Signals
signal rf_fwd_1 	  : std_logic := '0';
signal rf_fwd_2     : std_logic := '0';
signal rf_data1  	  : std_logic_vector (15 downto 0) := (others => '0');
signal rf_data2  	  : std_logic_vector (15 downto 0) := (others => '0');

signal alu_fwd_1	  : std_logic_vector (15 downto 0) := (others => '0');
signal alu_fwd_2	  : std_logic_vector (15 downto 0) := (others => '0');
signal aluin_data1  : std_logic_vector (15 downto 0) := (others => '0');
signal aluin_data2  : std_logic_vector (15 downto 0) := (others => '0');
-- Constant
signal fwd_data_cons  	: std_logic_vector (15 downto 0) := (others => '1');
-- End of Forwards

signal controller_outport : std_logic_vector (15 downto 0) := (others => '1');

begin


-- Test 1
ROM: entity work.ROM port map (clk, fd_pc_out, fd_data, fd_pc_in);

-- Test 2
--ROM: entity work.rom_test_2 port map (clk, fd_pc_out, fd_data, fd_pc_in);

-- Test 3
--ROM: entity work.rom_test_3 port map (clk, fd_pc_out, fd_data, fd_pc_in);

-- PIPELINE STAGES

IFID: entity work.fetchdecode port map (clk,rst,fd_data,fd_pc_in,inport,br_pc,br_en, fd_pc_out,
													 rf_rd_index1,rf_rd_index2, rf_fwd_data1, rf_fwd_data2, rf_forward, rf_wr_enable,rf_wr_index,
													 rf_wr_data,rf_opcode,rf_cl,rf_br_base,rf_wb_index);
												  
REGFILE: entity work.register_file port map(rst, clk, rf_rd_index1, rf_rd_index2, rf_fwd_1, rf_fwd_2, rf_data1, rf_data2, rf_forward,
														  rf_wr_index, rf_wr_data, rf_wr_enable, wbb_index, wbb_data, wbb_en,rf_opcode, 
														  rf_wb_index, rf_cl, rf_br_base, alu_index1, alu_index2,
														  alu_in1, alu_in2, alu_fwd_info, alu_wb_index, alu_opcode, alu_cl, alu_br_base);
														 
											  
ALU: entity work.alu port map(rst, clk, alu_opcode, aluin_data1, aluin_data2, alu_cl, alu_br_base, alu_wb_index,
										mem_opcode, mem_data_in, mem_upper, mem_wb_index, mem_addr_in);
										
MEM: entity work.memory port map(clk, rst, mem_addr_in, mem_data_in, mem_opcode, mem_upper, mem_wb_index,
											dc_opcode, dc_wb_data, dc_wb_index);


DC: entity work.data_controller port map(rst, clk, dc_wb_data, dc_opcode, dc_wb_index,
													  wbb_index, wbb_data, wbb_en, wbb_opcode, br_en, br_pc, controller_outport); 		
			
-- END OF PIPELINE STAGES

-- Regifile input forwarding
rd1_forward: entity work.forward_mux port map(rf_fwd_data1, mem_data_in, dc_wb_data, wbb_data,
															 rf_opcode, mem_opcode, dc_opcode, wbb_opcode,
															 rf_rd_index1, mem_wb_index, dc_wb_index, wbb_index,
															 rf_data1);

rd2_forward: entity work.forward_mux port map(rf_fwd_data2, mem_data_in, dc_wb_data, wbb_data,
															 rf_opcode, mem_opcode, dc_opcode, wbb_opcode,
															 rf_rd_index2, mem_wb_index, dc_wb_index, wbb_index,
															 rf_data2);															 

rf_fwd_1 <= '1' when (rf_forward(0) = '1') and (rf_data1 /= fwd_data_cons) else '0';
rf_fwd_2 <= '1' when (rf_forward(1) = '1') and (rf_data2 /= fwd_data_cons) else '0';


-- ALU input forwarding
alu1_forward : entity work.forward_mux port map(alu_in1, mem_data_in, dc_wb_data, wbb_data, 
																alu_opcode, mem_opcode, dc_opcode, wbb_opcode,
															   alu_index1, mem_wb_index, dc_wb_index, wbb_index,
																alu_fwd_1);
																
alu2_forward : entity work.forward_mux port map(alu_in2, mem_data_in, dc_wb_data, wbb_data,
																alu_opcode, mem_opcode, dc_opcode, wbb_opcode,
															   alu_index2, mem_wb_index, dc_wb_index, wbb_index,
																alu_fwd_2);
																
aluin_data1 <= alu_fwd_1 when (alu_fwd_info(0) = '1') else alu_in1;
aluin_data2 <= alu_fwd_2 when (alu_fwd_info(1) = '1') else alu_in2;

-- Set Output
outport <= controller_outport;

end Behavioral;

