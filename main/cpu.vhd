
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity cpu is
port (
	clk, rst 		: in std_logic;
	input_switch 	: in std_logic_vector (7 downto 0);
	an 				: out std_logic_vector(3 downto 0);
	sseg 				: out std_logic_vector(6 downto 0)
);
end cpu;

architecture Behavioral of cpu is

signal sign_extension 	: std_logic_vector (7 downto 0) := (others => '0') ;
signal input_16_bit 		: std_logic_vector (15 downto 0) := (others => '0');
signal output   		   : std_logic_vector (15 downto 0) := (others => '0');
signal clk_prscale 		: std_logic := '0';

begin

sign_extension <= (others => input_switch(7));
input_16_bit <= sign_extension & input_switch;

controller : entity work.controller port map(clk, rst, input_16_bit, output);
prescaler  : entity work.display_prescaler port map(clk, rst, clk_prscale);
display    : entity work.display_controller port map(clk_prscale, rst, output(15 downto 12), output(11 downto 8), 
																	  output(7 downto 4), output(3 downto 0), 
																	  an, sseg);

--display    : entity work.display_controller port map(clk_prscale, rst, x"A", x"B", x"C",x"D", an, sseg);

end Behavioral;

