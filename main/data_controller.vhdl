library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;
use IEEE.NUMERIC_STD.ALL; 

entity data_controller is
port(
	-- System
	rst						: in std_logic;
	clk						: in std_logic;
	-- ALU Outputs
	alu_result_lower		: in std_logic_vector(15 downto 0);
	opcode					: in std_logic_vector(6 downto 0);
	wb_index_in				: in std_logic_vector(2 downto 0); 

	-- WBB Outputs
	wb_index_out			: out std_logic_vector(2 downto 0); 
	wb_data					: out std_logic_vector(15 downto 0); 
	wb_en						: out std_logic;
	wb_opcode				: out std_logic_vector(6 downto 0);
	-- Branching Outputs'
	br_en						: out std_logic;
	br_pc						: out std_logic_vector(15 downto 0);
	-- Controller Output
	controller_output    : out std_logic_vector(15 downto 0)	
);
end data_controller;

architecture Behavioral of data_controller is

-- Format A
constant NOP    	: integer := 0;
constant ADD    	: integer := 1;
constant SUB    	: integer := 2;
constant MUL    	: integer := 3;
constant NAAND  	: integer := 4;
constant SHL    	: integer := 5;
constant SHR    	: integer := 6;
constant TEST   	: integer := 7;
constant OUTPUT 	: integer := 32;
constant INPUT  	: integer := 33;

-- Format B
constant BRR 		: integer := 64;
constant BRRN 		: integer := 65;
constant BRRZ 		: integer := 66;
constant BR 		: integer := 67;
constant BRN 		: integer := 68;
constant BRZ 		: integer := 69;
constant BRSUB 		: integer := 70;
constant BRETURN 	: integer := 71;

-- Format L
constant LOAD	 	: integer := 16;
constant STORE  	: integer := 17;
constant LOADIMM	: integer := 18;
constant MOV		: integer := 19;

begin
process(clk)
begin
	if rising_edge(clk) then
		if (rst = '1') then
			wb_index_out			<= (others => '0');
			wb_data					<= (others => '0');
			wb_en						<= '0';
			controller_output    <= (others => '0');
			br_en						<= '0';
			br_pc						<= (others => '0');
		else			
			-- Handle Data from ALU
			wb_opcode <= opcode;
			case to_integer(unsigned(opcode(6 downto 0))) is
				when ADD | SUB | NAAND | SHL | SHR | MUL | MOV | LOADIMM | LOAD =>
					wb_en					<= '1';
					wb_index_out		<= wb_index_in;
					wb_data 				<= alu_result_lower;
					-- branching
					br_en				<= '0';
					br_pc				<= (others => '0');					
					
				when BR | BRR | BRSUB | BRETURN | BRRZ | BRZ | BRRN | BRN =>
				   if (opcode = "1000110") then
						wb_en				<= '1';
						wb_index_out   <= wb_index_in;
					else
						wb_en				<= '0';
						wb_data			<= (others => '0');
					end if;
					br_en				<= '1';
					br_pc				<= alu_result_lower;
					
				
				when OUTPUT =>
					wb_en					<= '0';
					wb_index_out		<= (others => '0');
					wb_data 				<= (others => '0');
					br_en					<= '0';
					br_pc					<= (others => '0');
					controller_output <= alu_result_lower; 
				when others =>
					wb_en					<= '0';
					wb_index_out		<= (others => '0');
					wb_data 				<= (others => '0');
					--controller_output 	<= (others => '0');
					br_en					<= '0';
					br_pc					<= (others => '0');					
			end case;
		end if;
	end if;
end process;
end Behavioral;

