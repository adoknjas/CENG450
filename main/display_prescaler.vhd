
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity display_prescaler is
port(
		clk_in, reset: in std_logic;
		clk_out      : out std_logic
);
end display_prescaler;

architecture Behavioral of display_prescaler is

signal prescale_value : integer;
signal clk_buffer : std_logic;

constant prescale    	: integer := 50000;

begin

clk_out <= clk_buffer;

process(clk_in)
begin
	if rising_edge(clk_in) then
		if (reset = '1') then
			prescale_value <= prescale;
			clk_buffer <= '0';
		else
			if (prescale_value = 0) then
				prescale_value <= prescale;
				clk_buffer <= not clk_buffer;
			else
				prescale_value <= prescale_value - 1;
			end if;
		end if;
	end if;
end process;


end Behavioral;

