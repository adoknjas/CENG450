
library IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_SIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity execute is
    port(
			--System
         clk            	: in std_logic;
			rst 	   	   	: in std_logic;
			-- Reg File Inputs
			rd_data1       	: in std_logic_vector (15 downto 0);
			rd_data2       	: in std_logic_vector (15 downto 0);
			-- misc inputs
			wb_index      	: in std_logic_vector (2 downto 0);
			opcode      	: in std_logic_vector (6 downto 0);
			cl 			   	: in std_logic_vector (15 downto 0);
			in_br_base 	   	: in std_logic_vector (15 downto 0);
			
			-- Outputs (ALU related)
			alu_opcode	 	: out std_logic_vector (6 downto 0);
			alu_in1        	: out std_logic_vector (15 downto 0);
			alu_in2        	: out std_logic_vector (15 downto 0);
			
			-- data controller related
			dc_wb_index		: out std_logic_vector(2 downto 0);
			dc_mem_addr		: out std_logic_vector(15 downto 0)		
         );
end execute;

architecture Behavioral of execute is
-- Signal

-- Special Instructions Handlers
constant SHL    	: integer := 5;
constant SHR    	: integer := 6;
constant LOAD	 	: integer := 16;
constant STORE	 	: integer := 17;
constant LOADIMM	: integer := 18;

constant BRR 		: integer := 64;
constant BRRN 		: integer := 65;
constant BRRZ 		: integer := 66;
constant BR 		: integer := 67;
constant BRN 		: integer := 68;
constant BRZ 		: integer := 69;
constant BRSUB 	: integer := 70;
constant BRETURN 	: integer := 71;

begin

process(clk)
begin
	if rising_edge(clk) then
		if (rst = '1') then
			alu_opcode <= (others => '0');
			alu_in1 <= (others => '0');
			alu_in2 <= (others => '0');
			dc_wb_index <= (others => '0');
			dc_mem_addr <= (others => '0');
		else 
			alu_opcode <= opcode;
			dc_wb_index <= wb_index;
			case to_integer(unsigned(opcode(6 downto 0))) is
				-- SHL, SHR and LOADIMM. The only things which use the CL port
				when SHL | SHR | LOADIMM =>
					alu_in1 <= rd_data1;
					alu_in2 <= cl;
					
				when BRRN | BRRZ =>
					alu_in1 <= in_br_base;
					alu_in2 <= cl;
					
				when BR | BRN | BRZ | BRSUB =>
					alu_in1 <= in_br_base;
					alu_in2 <= rd_data1;
					
				when BRETURN =>
					alu_in1 <= rd_data1;
					
				-- LOAD from MEM to Registers
				when LOAD =>
					dc_mem_addr <= rd_data1;
					
				-- STORE from Reg to MEM
				when STORE =>
					alu_in1 <= rd_data1;
					dc_mem_addr <= rd_data2;
				
				-- Everything Else i.e No Main Memory operations
				when others =>
					alu_in1 <= rd_data1;
					alu_in2 <= rd_data2;
					dc_wb_index <= wb_index;
			end case;
		end if;
	end if;
end process;
end Behavioral;

