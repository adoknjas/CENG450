
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_SIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity fetchdecode is
    port(
			--in
         	clk       	: in std_logic;
			rst 		 	: in std_logic;
         	data      	: in std_logic_vector (15 downto 0);
			pc_in			: in std_logic_vector (6 downto 0);
			inport    	: in std_logic_vector (15 downto 0);
			-- Branches
			br_pc 		: in std_logic_vector (15 downto 0);
			br_en			: in std_logic;
			-- out to Register File
			pc_out   	: out std_logic_vector (6 downto 0);
			rd_index1 	: out std_logic_vector (2 downto 0);
			rd_index2 	: out std_logic_vector (2 downto 0);
			rd_fwd_1 	: out std_logic_vector (15 downto 0);
			rd_fwd_2 	: out std_logic_vector (15 downto 0);
			forward	   : out std_logic_vector (1 downto 0);
			-- Writing Operands
			wr_enable 	: out std_logic;
			wr_index  	: out std_logic_vector (2 downto 0);
			wr_data   	: out std_logic_vector (15 downto 0);
			-- To Execution stage
			opcode    	: out std_logic_vector (6 downto 0);
			cl        	: out std_logic_vector (15 downto 0);
			br_base 		: out std_logic_vector (15 downto 0);
			-- To Data controller
			wb_index  	: out std_logic_vector (2 downto 0)
         );
end fetchdecode;

architecture Behavioral of fetchdecode is

-- Format A Instructions
constant NOP    	: integer := 0;
constant ADD    	: integer := 1;
constant SUB    	: integer := 2;
constant MUL    	: integer := 3;
constant NAAND  	: integer := 4;
constant SHL    	: integer := 5;
constant SHR    	: integer := 6;
constant TEST   	: integer := 7;
constant OUTPUT 	: integer := 32;
constant INPUT  	: integer := 33;

-- Format B Instructions
constant BRR 		: integer := 64;
constant BRRN 		: integer := 65;
constant BRRZ 		: integer := 66;
constant BR 		: integer := 67;
constant BRN 		: integer := 68;
constant BRZ 		: integer := 69;
constant BRSUB 	: integer := 70;
constant BRETURN 	: integer := 71;

-- Format L Instructions
constant LOAD	 	: integer := 16;
constant STORE  	: integer := 17;
constant LOADIMM	: integer := 18;
constant MOV		: integer := 19;

begin

process(clk)
variable pc 				: integer := 0;
variable nop_counter		: integer := 0;
begin
	if rising_edge(clk) then
		if (rst = '1') then
			wr_enable <= '0';
			wr_index <= (others => '0');
			wr_data <= (others => '0');
			rd_index1 <= (others => '0');
			rd_index2 <= (others => '0');
			wb_index <= (others => '0');
			opcode <= (others => '0');
			cl <= (others => '0');
			br_base <= (others => '0');
			--pc := 0;
			pc := to_integer(unsigned(pc_in));
			pc_out <= std_logic_vector(to_unsigned(pc, pc_out'length));
			forward <= "00";
			rd_fwd_1 <= x"ffff";
			rd_fwd_2 <= x"ffff";
		elsif (br_en = '1') then
			pc := to_integer(unsigned(br_pc));
			pc_out <= std_logic_vector(to_unsigned(pc, pc_out'length));
			opcode <= (others => '0');
		elsif (nop_counter > 0) then
			wr_enable <= '0';
			wr_index <= (others => '0');
			wr_data <= (others => '0');
			rd_index1 <= (others => '0');
			rd_index2 <= (others => '0');
			wb_index <= (others => '0');
			opcode <= (others => '0');
			cl <= (others => '0');
			br_base <= (others => '0');
			forward <= "00";
			--pc_out <= std_logic_vector(to_unsigned(pc, pc_out'length));
			pc_out <= std_logic_vector(to_unsigned(0, pc_out'length));
			nop_counter	:= nop_counter - 1;
		else
			opcode <= data(15 downto 9);
			wr_index <= (others => '0');
			case to_integer(unsigned(data(15 downto 9))) is		
				-- Format A1 instructions
				when ADD | SUB | MUL | NAAND  =>
					wr_enable <= '0';
					wr_data <= (others => '0');
					wb_index <=  data(8 downto 6);
					rd_index1 <= data(5 downto 3);
					rd_index2 <= data(2 downto 0);
					cl <= (others => '0');
					forward <= "11";
				
				-- Format A2 instructions
				when SHL | SHR =>
					wr_enable <= '0';
					wr_data <= (others => '0');
					wb_index  <= data(8 downto 6);
					rd_index1 <= data(8 downto 6);
					cl 		 <= (15 downto 4 => '0') & data(3 downto 0);
					forward <= "01";
					
				-- Format A3 instructions
				when TEST =>
					wr_enable <= '0';
					wr_data <= (others => '0');
					wb_index <= (others => '0');
					rd_index1 <= data(8 downto 6);
					cl <= (others => '0');
					forward <= "01";
					
				when OUTPUT =>
					wr_enable <= '0';
					wr_data <= (others => '0');
					wb_index <= (others => '0');
					rd_index1 <= data(8 downto 6);
					cl <= (others => '0');
					forward <= "01";
				
				when INPUT =>
					wr_data <= inport;
					wr_index <= data(8 downto 6);
					wb_index <= (others => '0');
					cl <= (others => '0');
					wr_enable <= '1';
					
				-- FORMAT B1 Instructions					
				when  BRR | BRRZ | BRRN =>
					wr_enable <= '0';
					wr_data <= (others => '0');
					br_base   <= std_logic_vector(to_signed(pc-2, br_base'length));
					cl 		  <= std_logic_vector(resize(signed(data(8 downto 0)),cl'length));
					wb_index <= (others => '0');
					nop_counter := 4;
					forward <= "00";

				when BR | BRN | BRZ =>
					wr_enable <= '0';
					wr_data <= (others => '0');
					rd_index1 <= data(8 downto 6);
					wb_index <= (others => '0');
					br_base   <= std_logic_vector(resize(signed(data(5 downto 0)),br_base'length));
					cl   <= std_logic_vector(to_signed(pc-2, cl'length));
					nop_counter := 4;
					forward <= "01";
					
				when BRSUB =>
					wr_enable <= '0';
					wb_index  <= "111";
					cl   <= std_logic_vector(to_unsigned(pc+1, wr_data'length));
					rd_index1 <= data(8 downto 6);
					wb_index <= (others => '0');
					br_base   <= std_logic_vector(resize(signed(data(5 downto 0)),br_base'length));
					nop_counter := 4;
					forward <= "01";
					
				when BRETURN =>
					wr_enable <= '0';
					rd_index1 <= "111";
					wb_index <= (others => '0');
					wr_data <= (others => '0');
					nop_counter := 4;
					cl <= (others => '0');
					forward <= "01";
				
				--Format L1 Instructions
				when LOADIMM =>
					wr_enable <= '0';
					wr_data <= (others => '0');
					wb_index <= "111"; -- Register R7
					rd_index1 <= "111";
					cl <= (15 downto 8 => data(8)) & data(7 downto 0);
					forward <= "01";
				
				--FORMAT L2 Instructions
				when LOAD | MOV =>
					wr_enable <= '0';
					wr_data <= (others => '0');
					wb_index <= data(8 downto 6);
					rd_index1 <= data(5 downto 3);
					cl <= (others => '0');
					forward <= "01";
				
				when STORE =>
					wr_enable <= '0';
					wr_data <= (others => '0');
					rd_index1 <= data(5 downto 3);
					rd_index2 <= data(8 downto 6);
					wb_index <= (others => '0');
					cl <= (others => '0');
					forward <= "11";
					
				when others => 
					wr_enable <= '0';
					wr_data <= (others => '0');
					wr_index <= (others => '0');
					wr_data <= (others => '0');
					rd_index1 <= (others => '0');
					rd_index2 <= (others => '0');
					wb_index <= (others => '0');
					opcode <= (others => '0');
					cl <= (others => '0');
					forward <= "00";
			end case;
			pc := pc + 1;
			pc_out <= std_logic_vector(to_unsigned(pc, pc_out'length));
			
		end if;		
	end if;
end process;

end Behavioral;

