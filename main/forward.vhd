library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity forward_mux is
	port(
		in1, in2, in3, in4: in std_logic_vector(15 downto 0);
		sel_opcode_1, sel_opcode_2, sel_opcode_3, sel_opcode_4 : in std_logic_vector(6 downto 0);
		sel_wb_index1, sel_wb_index2, sel_wb_index3, sel_wb_index4 : in std_logic_vector(2 downto 0);
		out0 : out std_logic_vector(15 downto 0)
		
	);
end forward_mux;

architecture Behavioral of forward_mux is

signal out3 : std_logic_vector (15 downto 0) := (others => '0');
signal out2 : std_logic_vector (15 downto 0) := (others => '0');
signal out1 : std_logic_vector (15 downto 0) := (others => '0');
begin

MUX0: entity work.forward_mux_in port map (in1, in4, sel_opcode_1, sel_opcode_4, sel_wb_index1, sel_wb_index4, out3);
MUX1: entity work.forward_mux_in port map (out3, in3, sel_opcode_1, sel_opcode_3, sel_wb_index1, sel_wb_index3, out2);
MUX2: entity work.forward_mux_in port map (out2, in2, sel_opcode_1, sel_opcode_2, sel_wb_index1, sel_wb_index2, out1);

out0 <= out1 when (sel_opcode_2 /= "0010000") else out2; -- don't use a load happening before memory, because its value is undefined

end Behavioral;
