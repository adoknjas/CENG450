
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.NUMERIC_STD.ALL;

-- Format A Instructions

entity forward_mux_in is
	port(
		in1, in2: in std_logic_vector(15 downto 0);
		sel_opcode_1, sel_opcode_2 : in std_logic_vector(6 downto 0);
		sel_wb_index1, sel_wb_index2 : in std_logic_vector(2 downto 0);
		out1 : out std_logic_vector(15 downto 0)
		
	);
end forward_mux_in;

architecture Behavioral of forward_mux_in is
constant ADD    	: integer := 1;
constant SUB    	: integer := 2;
constant MUL    	: integer := 3;
constant NAAND  	: integer := 4;
constant SHL    	: integer := 5;
constant SHR    	: integer := 6;
constant TEST   	: integer := 7;
constant OUTPUT 	: integer := 32;

-- Format B Instructions
constant BR 		: integer := 67;
constant BRN 		: integer := 68;
constant BRZ 		: integer := 69;
constant BRSUB 	: integer := 70;
constant BRETURN 	: integer := 71;

-- Format L Instructions
constant LOAD	 	: integer := 16;
constant STORE  	: integer := 17;
constant LOADIMM	: integer := 18;
constant MOV		: integer := 19;

function IS_WRITER (opcode : in std_logic_vector(6 downto 0)) return std_logic is
	begin
	case to_integer(unsigned(opcode)) is 
		when ADD | SUB | MUL | NAAND | SHL | SHR | BRSUB | LOADIMM | LOAD | MOV =>  return '1';
		when others => return '0';
	end case;
end function;


function IS_READER (opcode : in std_logic_vector(6 downto 0)) return std_logic is
	begin
	case to_integer(unsigned(opcode)) is 
		when ADD | SUB | MUL | NAAND | SHL | SHR | TEST | OUTPUT | BR | BRN | BRZ | BRSUB | BRETURN | LOAD | STORE | LOADIMM | MOV => return '1';
		when others => return '0';
	end case;
end IS_READER;

begin

out1 <= in2 when (IS_WRITER(sel_opcode_2) = '1') and (IS_READER(sel_opcode_1) = '1') and (sel_wb_index1 = sel_wb_index2)
				else in1;

end Behavioral;


