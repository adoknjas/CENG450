
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity latch_execute is
port(
	-- System
	clk							: in std_logic;
	rst							: in std_logic;

	-- ALU Outputs
	in_alu_opcode	 			: in std_logic_vector (6 downto 0);
	in_alu_in1        		: in std_logic_vector (15 downto 0);
	in_alu_in2        		: in std_logic_vector (15 downto 0);
			
	-- data controller related
	in_wb_index					: in std_logic_vector(2 downto 0);
	in_mem_addr					: in std_logic_vector(15 downto 0);

	--inputs
	out_alu_opcode				: out std_logic_vector(6 downto 0);
	out_alu_in1					: out std_logic_vector(15 downto 0);
	out_alu_in2					: out std_logic_vector(15 downto 0);
	
	--Data controller things
	out_wb_index				: out std_logic_vector(2 downto 0);
	out_mem_addr				: out std_logic_vector(15 downto 0)
	
);
end latch_execute;

architecture Behavioral of latch_execute is
signal buff_alu_opcode 	: std_logic_vector(6  downto 0) := (others => '0');
signal buff_alu_in1 		: std_logic_vector(15 downto 0) := (others => '0');
signal buff_alu_in2 		: std_logic_vector(15 downto 0) := (others => '0');
signal buff_wb_index 	: std_logic_vector(2 downto 0) := (others => '0');
signal buff_mem_addr 	: std_logic_vector(15 downto 0) := (others => '0');

begin

process(clk)
begin
	if rising_edge(clk) then
		if (rst = '1') then 
			out_alu_opcode	<= (others => '0');
			out_alu_in1		<= (others => '0');
			out_alu_in2		<= (others => '0');
			out_wb_index	<= (others => '0');
			out_mem_addr	<= (others => '0');
		else
			out_alu_opcode	<= buff_alu_opcode;
			out_alu_in1		<= buff_alu_in1;
			out_alu_in2		<= buff_alu_in2;
			out_wb_index	<= buff_wb_index;
			out_mem_addr	<= buff_mem_addr;
			--buffer incoming
			buff_alu_opcode <= in_alu_opcode;
			buff_alu_in1	 <= in_alu_in1;
			buff_alu_in2	 <= in_alu_in2;
			buff_wb_index	 <= in_wb_index;
			buff_mem_addr	 <= in_mem_addr;
		end if;
	end if;
end process;
end Behavioral;

