
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity latch_fetch is
port (
--system ports
rst : in std_logic;
clk: in std_logic;

-- latch in
in_rd_index1 : in std_logic_vector(2 downto 0);
in_rd_index2 : in std_logic_vector(2 downto 0);
in_wr_index  : in std_logic_vector(2 downto 0);
in_wr_data   : in std_logic_vector(15 downto 0);
in_wr_en 	 : in std_logic;
in_opcode 	 : in std_logic_vector(6 downto 0);
in_wb_index  : in std_logic_vector(2 downto 0);
in_cl 		 : in std_logic_vector(15 downto 0);
in_br_base 	 : in std_logic_vector (15 downto 0);

-- latch out
out_rd_index1 : out std_logic_vector(2 downto 0);
out_rd_index2 : out std_logic_vector(2 downto 0);
out_wr_index  : out std_logic_vector(2 downto 0);
out_wr_data   : out std_logic_vector(15 downto 0);
out_wr_en  	  : out std_logic;
out_opcode 	  : out std_logic_vector(6 downto 0);
out_wb_index  : out std_logic_vector(2 downto 0);
out_cl 		  : out std_logic_vector(15 downto 0);
out_br_base	  : out std_logic_vector (15 downto 0)
);
end latch_fetch;

architecture Behavioral of latch_fetch is
signal buff_rd_index1 : std_logic_vector(2 downto 0) := (others => '0');
signal buff_rd_index2 : std_logic_vector(2 downto 0) := (others => '0');
signal buff_wr_index  : std_logic_vector(2 downto 0) := (others => '0');
signal buff_wr_data 	 : std_logic_vector(15 downto 0) := (others => '0');
signal buff_wr_en 	 : std_logic := '0';
signal buff_opcode 	 : std_logic_vector(6 downto 0) := (others => '0');
signal buff_wb_index  : std_logic_vector(2 downto 0) := (others => '0');
signal buff_cl 		 : std_logic_vector(15 downto 0) := (others => '0');
signal buff_br_base 	 : std_logic_vector(15 downto 0) := (others => '0');

begin
process(clk)
begin
	if rising_edge(clk) then
		if (rst = '1') then
			-- Set all outputs to low 
			out_rd_index1 <= (others => '0');
			out_rd_index2 <= (others => '0');
			out_wr_index <= (others => '0');
			out_wr_data <= (others => '0');
			out_wr_en <= '0';
			out_opcode <= (others => '0');
			out_wb_index <= (others => '0');
			out_cl <= (others => '0');
			out_br_base <= (others => '0');
			-- clear out the buffers
			buff_rd_index1 <= (others => '0');
			buff_rd_index2 <= (others => '0');
			buff_wr_index <= (others => '0');
			buff_wr_data <= (others => '0');
			buff_wr_en <= '0';
			buff_opcode <= (others => '0');
			buff_wb_index <= (others => '0');
			buff_cl <= (others => '0');
			buff_br_base <= (others => '0');
		else
			-- Send out buffered data
			out_rd_index1 <= buff_rd_index1;
			out_rd_index2 <= buff_rd_index2;
			out_wr_index <= buff_wr_index;
			out_wr_data <= buff_wr_data;
			out_wr_en <= buff_wr_en;
			out_opcode <= buff_opcode;
			out_wb_index <= buff_wb_index;
			out_cl <= buff_cl;
			out_br_base <= buff_br_base;
			
			-- Buffer data
			buff_rd_index1 <= in_rd_index1;
			buff_rd_index2 <= in_rd_index2;
			buff_wr_index <= in_wr_index;
			buff_wr_data <= in_wr_data;
			buff_wr_en <= in_wr_en;
			buff_opcode <= in_opcode;
			buff_wb_index <= in_wb_index;
			buff_cl <= in_cl;
			buff_br_base <= in_br_base;
		end if;
	end if;
end process;


end Behavioral;

