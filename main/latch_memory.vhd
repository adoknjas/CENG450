library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity latch_dc is
port(
	-- System
	rst							: in std_logic;
	clk							: in std_logic;
	
	-- WBB Input
	in_wb_data_mem_wait		: in std_logic;
	in_wb_index					: in std_logic_vector(2 downto 0); 
	in_wb_data					: in std_logic_vector(15 downto 0); 
	in_wb_en						: in std_logic;
	-- Data In from Memory
	mem_data_in					: in std_logic_vector(15 downto 0); 
	-- WBB OUTPUTS
	out_wb_index				: out std_logic_vector(2 downto 0); 
	out_wb_data					: out std_logic_vector(15 downto 0); 
	out_wb_en					: out std_logic
);
end latch_dc;

architecture Behavioral of latch_dc is

signal buff_wb_en    : std_logic := '0';
signal buff_wb_index : std_logic_vector (2 downto 0) := (others => '0');
signal buff_wb_data  : std_logic_vector (15 downto 0) := (others => '0');

begin

process(clk)
begin
	if rising_edge(clk) then
		if (rst = '1') then
			out_wb_index <= (others => '0');
			out_wb_data	 <= (others => '0');
			out_wb_en	 <= '0';
		else
			out_wb_index <= buff_wb_index;
			out_wb_en	 <= buff_wb_en;
			
			if (buff_wb_mem = '1') then
				out_wb_data <= mem_data_in;
			else
				out_wb_data <= buff_wb_data;
			end if;
			
			buff_wb_en    <= in_wb_en;
			buff_wb_mem   <= in_wb_data_mem_wait;
			buff_wb_index <= in_wb_index;
			buff_wb_data  <= in_wb_data;
		end if;
	end if;
end process;

end Behavioral;

