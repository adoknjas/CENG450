library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_SIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

entity MEMORY is
    port(	--System
         	clk      		: in  std_logic;
			rst      		: in  std_logic;

			-- Memory Data 
         	mem_addr     	: in  std_logic_vector 	(15 downto 0); 
			in_data1 		: in std_logic_vector 	(15 downto 0);

			-- Just passing through
			in_op_code 		: in std_logic_vector 	(6 downto 0);
			in_data2 		: in std_logic_vector 	(15 downto 0);
			in_wb_index 	: in std_logic_vector 	(2 downto 0);

			-- Data Out to WriteBack stageaddr
			out_op_code 	: out std_logic_vector 	(6 downto 0);
			out_data 		: out std_logic_vector 	(15 downto 0);
			out_wb_index	: out std_logic_vector 	(2 downto 0)
        );
end MEMORY;

architecture Behavioral of MEMORY is
-- Addresses up to 2 * 2^16 because it is byte addressable
-- [2*mem_addr, 2*mem_addr+1] are returned in Big endian order
type RAM_TYPE is array (0 to 63) of std_logic_vector (7 downto 0);
signal ram_content 	: RAM_TYPE;
	
-- Format A Instructions
constant NOP    	: integer := 0;

-- Format L Instructions
constant LOAD	 	: integer := 16;
constant STORE  	: integer := 17;

begin

process(clk)
variable addr_int : integer := 0;
begin
	if rising_edge(clk) then
		if (rst = '1') then
			out_op_code <= (others => '0');
			out_data <= (others => '0');
			out_wb_index <= (others => '0');
			for i in 0 to 31 loop
				ram_content(i)<= (others => '0');
			end loop;
		else
			addr_int := 2 * conv_integer(mem_addr(5 downto 0));
			out_op_code <= in_op_code;
			out_wb_index <= in_wb_index;
			out_data <= in_data1;
			case (to_integer(unsigned(in_op_code))) is 
				when NOP =>
					out_op_code <= (others => '0');
					out_data 	 <= (others => '0');
					out_wb_index	 <= (others => '0');
					
				when LOAD =>
					-- Return as Big endian
					out_data <= ram_content(addr_int) & ram_content(addr_int+1);
					
				when STORE =>
					-- Store as big endian
					ram_content(addr_int) <= in_data1(15 downto 8);
					ram_content(addr_int+1) <= in_data1(7 downto 0);
					out_wb_index <= (others => '0');
					out_data <= (others => '0');
					
				when others =>
					NULL;
			end case;
		end if;
	end if;
end process;

end Behavioral;