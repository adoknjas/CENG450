--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   18:58:10 02/05/2018
-- Design Name:   
-- Module Name:   C:/Users/adoknjas/Documents/CENG450/CENG450/main/test_alu.vhd
-- Project Name:  CENG450
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: alu
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test_alu IS
END test_alu;
 
ARCHITECTURE behavior OF test_alu IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT alu
    PORT(
         rst : IN  std_logic;
         clk : IN  std_logic;
         alu_mode : IN  std_logic_vector(6 downto 0);
         in1 : IN  std_logic_vector(15 downto 0);
         in2 : IN  std_logic_vector(15 downto 0);
         z_flag : OUT  std_logic;
         n_flag : OUT  std_logic;
         result : OUT  std_logic_vector(15 downto 0);
			result_upper : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;

   --Inputs
   signal rst : std_logic := '0';
   signal clk : std_logic := '0';
   signal alu_mode : std_logic_vector(2 downto 0) := (others => '0');
   signal in1 : std_logic_vector(15 downto 0) := (others => '0');
   signal in2 : std_logic_vector(15 downto 0) := (others => '0');

 	--Outputs
   signal z_flag : std_logic;
   signal n_flag : std_logic;
   signal result : std_logic_vector(15 downto 0);
	signal result_upper : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 20 us;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: alu PORT MAP (
          rst => rst,
          clk => clk,
          alu_mode => alu_mode,
          in1 => in1,
          in2 => in2,
          z_flag => z_flag,
          n_flag => n_flag,
          result => result,
			 result_upper => result_upper
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- Init
		rst <= '1';
		alu_mode <= "000";
		in1 <= (others => '0');
		in2 <= (others => '0');
		
		wait until (clk='0' and clk'event);
		wait until (clk='1' and clk'event);
		wait until (clk='1' and clk'event);
		
		rst <= '0';
			
		--MUL 2 x 2
		wait until (clk='1' and clk'event);
		in1 <= "0000000000000010";
		in2 <= "0000000000000010";
		alu_mode <= "011";
		wait until (clk='1' and clk'event);
		wait until (clk='1' and clk'event);
		rst <= '1';
		wait until (clk='1' and clk'event);
		wait until (clk='1' and clk'event);
		rst <= '0';
		
		--MUL 2^16 * 2^16
		wait until (clk='1' and clk'event);
		in1 <= "0111111111111111"; -- 0x7fff largest 16 bit positive integer
		in2 <= "0111111111111111";
		alu_mode <= "011";
		wait until (clk='1' and clk'event);
		wait until (clk='1' and clk'event);
		rst <= '1';
		wait until (clk='1' and clk'event);
		wait until (clk='1' and clk'event);
		rst <= '0';
		
	
      --wait;
   end process;

END;
