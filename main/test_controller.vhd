-- TestBench Template 

  LIBRARY ieee;
  USE ieee.std_logic_1164.ALL;
  USE ieee.numeric_std.ALL;

  ENTITY test_controller IS
  END test_controller;

  ARCHITECTURE behavior OF test_controller IS 

  -- Component Declaration
          COMPONENT controller
          PORT(
					clk	  : in std_logic;
					rst 	  : in std_logic;
					inport  : in std_logic_vector(15 downto 0);
					outport : out std_logic_vector(15 downto 0)
					);
          END COMPONENT;

          SIGNAL clk, rst :  std_logic;
          SIGNAL inport, outport :  std_logic_vector(15 downto 0);
  BEGIN

  -- Component Instantiation
          c0: controller PORT MAP(
                  clk => clk,
                  rst => rst,
						inport => inport,
						outport => outport	
          );


	-- CLK
	PROCESS BEGIN
	clk <= '0'; wait for 10 us;
	clk<='1'; wait for 10 us; 
	END PROCESS;
	-- END CLK

  --  Test Bench Statements
     PROCESS BEGIN
		  rst <= '1';
		  inport <= x"0000";
        wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  rst <= '0';
		  inport <= x"0004";  -- write 3 to input
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  wait until (clk='1' and clk'event);
		  
        -- Add user defined stimulus here

        wait; -- will wait forever
     END PROCESS;
  --  End Test Bench 

  END;
